// const CONTAINER = $('#container');
const GREYSCALE_BUTTON = $('#greyscaleButton');
const HIDE_CLASS = 'd-none';
const ACTIVE_CIRCLE_CLASS = 'active';
const CIRCLE_BAR = $('#circleBar');
let index = 0;
let isAllowPageChange = true;
const pages = $('.page');
const fade = 'fast';

function nextPage() {
    let newIndex = index + 1;
    if (newIndex < pages.length) {
        changePage(newIndex);
    }
}

function prevPage() {
    let newIndex = index - 1;
    if (index - 1 >= 0) {
        changePage(newIndex);
    }
}

function changePage(newIndex) {
    if (isAllowPageChange) {
        isAllowPageChange = false;
        getCurrentPage().fadeOut(fade, function () {
            setPageDisplay(getCurrentPage(), false);
            index = newIndex;
            handleCircleChange();
            setPageDisplay(getCurrentPage(), true);
            getCurrentPage().fadeIn(fade, function () {
                isAllowPageChange = true;
            });
        });
    }
}

function handleCircleChange() {
    let circles = $('.circle');

    if (circles.length > 0) {
        circles.removeClass(ACTIVE_CIRCLE_CLASS);
        $(circles[index]).addClass(ACTIVE_CIRCLE_CLASS);
    }
}

function getCurrentPage() {
    return $(pages[index]);
}

function setPageDisplay(page, isDisplay) {
    if (isDisplay) {
        page.removeClass(HIDE_CLASS);
    } else {
        page.addClass(HIDE_CLASS);
    }
}

function generatePageCircles() {
    if (pages.length >= 2) {
        for (let i = 0; i < pages.length; i++) {
            CIRCLE_BAR.append(`<div class="circle" data-index="${i}"></div>`);
        }
        handleCircleChange();
    }
}

function toggleGrayscale() {
    $('#forms').toggleClass('active');
}

$(window).on('keydown', function (event) {
    let key = event.key;
    if (key === 'ArrowLeft') {
        prevPage();
    } else if (key === 'ArrowRight') {
        nextPage();
    }
});

GREYSCALE_BUTTON.click(toggleGrayscale);

CIRCLE_BAR.on('click', '[data-index]', function () {
    changePage($(this).data('index'));
});

$(document).ready(function () {
    pages.addClass(HIDE_CLASS);
    generatePageCircles();
    setPageDisplay(getCurrentPage(), true);
});