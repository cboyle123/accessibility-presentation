# Web-based Presentation of "Accessibility in Application Design"
Showcases simple, effective methods to make web applications accessible to users with disabilities, and highlights how these methods benefit all users.

## Installation
This application runs on Python and the Flask framework. 
Create a virtual environment in the project top-level directory and run `pip install flask` to complete installation.

Front-end slides use jQuery, which is handled by a CDN.
